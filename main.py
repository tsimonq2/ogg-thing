#!/usr/bin/env python3

from subprocess import call
import feedparser

test_rss_url = "https://asknoah.fireside.fm/rss"
feed = feedparser.parse(test_rss_url)
mp3url = feed["items"][0]["links"][1]["href"]

# This is because no Python libraries have worked with this URL.
call(["wget", mp3url, "latest.mp3"])

# Remove index.html, don't know why it downloads...
# TODO: Make this apart of the above wget
call(["rm", "index.html"])
